import React, { Component } from 'react';
import axios from 'axios';
import { Cookies } from 'react-cookie';
import { ProgressBar } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';


class Upload extends Component {
	state = {
			selectedFiles: null,
			percentage: 0,
			stop:''
			
		}
   
	FileChangedHandler = (event) => {
		this.setState({
			selectedFiles: event.target.files
		});
		var a1  = new Date();
		document.getElementById("file").innerHTML = a1;
		axios.get(" http://54.81.159.231:32060/uploadapp/id")
		.then(response => {
				const cookie = new Cookies();
				let d = new Date();
				d.setTime(d.getTime() + (60*60*1000));
				cookie.set("upload_id", response.data, {path: "/", expires: d});
				
		});
		
	};
    
	FileUploadHandler = (event) => {
		event.preventDefault();
		const data = new FormData();
		const cookie = new Cookies();
		cookie.remove("upload_id");
		let selectedFiles = this.state.selectedFiles;
		
				if ( selectedFiles ) {
			for ( let i = 0; i < selectedFiles.length; i++ ) {
				data.append( 'file', selectedFiles[ i ], selectedFiles[ i ].name );
				console.log(data);
			}
			data.append("id",undefined);
			const options = {
			onUploadProgress: (progressEvent) => {
			const {loaded, total} = progressEvent;
			let percent = Math.floor( (loaded * 100) / total )
			console.log( `${loaded}kb of ${total}kb | ${percent}%` );

			if( percent < 100 ){
			this.setState({ uploadPercentage1: percent })
			console.log(this.setState({ uploadPercentage1: percent }));
			}
		}
		}  
		 axios.get(" http://54.81.159.231:32060/uploadapp/percentdelete")
		.then((response)=>{
			console.log("deleted",response);
		})
		    var current = new Date();
			console.log("start time",current);
			axios.post(  ' http://54.81.159.231:32060/uploadapp/s3', data,options,{
				headers: {
					"Access-Control-Allow-Origin": "*",
					"Access-Control-Allow-Methods": "POST", 
					'accept': 'application/json',
					'Accept-Language': 'en-US,en;q=0.8',
					'Content-Type': 'application/json'
					//'Content-Type': `multipart/form-data`
				}
			})
				.then( ( response ) => {
					console.log( 'res', response );
					 })
			
					 
					 var timer =  setInterval(() => {
						var url = " http://54.81.159.231:32060/uploadapp/percent";
                           this.getid(url);
                        }, 5000);
						this.setState({stop : timer});
						}
	};
	getid= async(url) => {
		await  
		axios.get(url)
	.then(response => {
	this.setState({percentage : parseInt(response.data)});
	});
	};

	FileUploadHandlerResume = (event) => {
		event.preventDefault();	
		const data = new FormData();
		const cookie = new Cookies();
	    const upload_id = cookie.get("upload_id");
		console.log(upload_id);
		data.append("id",upload_id);
		let selectedFiles = this.state.selectedFiles;
		
		if ( selectedFiles ) {
			for ( let i = 0; i < selectedFiles.length; i++ ) {
				data.append( 'file', selectedFiles[ i ], selectedFiles[ i ].name );
				console.log(data);
			}
			
		const options = {
			onUploadProgress: (progressEvent) => {
			const {loaded, total} = progressEvent;
			let percent = Math.floor( (loaded * 100) / total )
			console.log( `${loaded}kb of ${total}kb | ${percent}%` );

			if( percent < 100 ){
			this.setState({ uploadPercentage: percent })
			console.log(this.setState({ uploadPercentage: percent }));
			}
		}
		}
		    
			var current = new Date();
			console.log("start time",current);
			axios.post('http://54.81.159.231:32060/uploadapp/s3', data,options,{
				headers: {
					"Access-Control-Allow-Origin": "*",
					"Access-Control-Allow-Methods": "POST", 
					'accept': 'application/json',
					'Accept-Language': 'en-US,en;q=0.8',
					'Content-Type': 'application/json'
					//'Content-Type': `multipart/form-data`
				}
			})
				.then( ( response ) => {
					console.log("response",response);
				})
                
                        var timer =  setInterval(() => {
						var url = " http://54.81.159.231:32060/uploadapp/resumepercent";
                           this.getid(url);
                        }, 5000);
						this.setState({stop : timer});
                
            }
	};
	getid= async(url) => {
		await  
		axios.get(url)
	.then(response => {
	this.setState({percentage : parseInt(response.data)});
	});
	};
    
    render() {
		
		const percentage = this.state.percentage;
        console.log('progress ',this.state.percentage);
		const stop = this.state.stop;
		if(percentage === 100){
			clearInterval(stop);
		}
		return(
			<div className="container">
				<div id="oc-alert-container"></div>
				
				<div className="card border-light mb-3">
					<div className="card-header">
						<h3 style={{ color: '#555', marginLeft: '12px' }}>Upload Files</h3>
						<p className="text-muted" style={{ marginLeft: '12px' }}></p>
					</div>
					<div className="card-body">
					
						<p className="card-text">Please upload the files</p>
					
						<p id="file"></p>
						
						<input type="file" onChange={this.FileChangedHandler}/>
						<div>
					    <div className="mt-5">
						
						<button type="button" onClick={this.FileUploadHandler}>Upload</button>
						
						{/*{ uploadPercentage1 > 0 && <ProgressBar now={uploadPercentage1} active label={`${uploadPercentage1}%`} /> }*/}
						
						<p></p>
						<button type="button" onClick={this.FileUploadHandlerResume}>Resume</button>
						{/*{ uploadPercentage > 0 && <ProgressBar now={uploadPercentage} active label={`${uploadPercentage}%`} /> }	*/}
						 <p></p>
						 <ProgressBar now={percentage} active label={`${percentage}%`} /> 
						</div>
					 </div>
					</div>
				</div>
			</div>
		);
	}
}

export default Upload;
       
        
   
                       
						
						